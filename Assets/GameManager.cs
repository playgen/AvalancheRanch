﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{

    public static float TimeToRockFall = 150f;
    private static float _time = 150f;
    public GameObject Gate;

    public Text TimeText;
    public Text HealthText;

    public RanchManager Ranch;

    public static bool GameStarted;
	// Use this for initialization
	void Start ()
	{
	    TimeText.text = "";
	    HealthText.text = "";
	    Gate.SetActive(true);
	    GameStarted = false;
	    TimeToRockFall = _time;
	}
	
	// Update is called once per frame
	void Update () {
	    if (GameStarted)
	    {
	        if (TimeToRockFall <= 0f && Gate.activeSelf)
	        {
	            Gate.SetActive(false);
	            TimeToRockFall = 0f;
	        }
	        else if (Gate.activeSelf)
	        {
	            TimeToRockFall -= Time.deltaTime;
	        }
            TimeText.text = Mathf.RoundToInt(TimeToRockFall) + "s";
            HealthText.text = "Health: " + Ranch.GetStatus() + "%";
        }
	}

    public static void StartGame(int players)
    {
        TimeToRockFall /= players;
        GameStarted = true;
        _time = TimeToRockFall;
    }
}
