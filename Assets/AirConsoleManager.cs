﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using NDream.AirConsole;
using Newtonsoft.Json.Linq;
using UnityEngine.SceneManagement;

public class AirConsoleManager : MonoBehaviour
{

    public PlayerController PlayerController;

    public int PlayersRequired = 2;

    void Awake()
    {
        AirConsole.instance.onMessage += OnMessage;
        AirConsole.instance.onConnect += OnConnect;
        //AirConsole.instance.onDisconnect += OnDisconnect;

    }

    void Start()
    { 
        
        if (AirConsole.instance != null && AirConsole.instance.GetControllerDeviceIds().Count > 0)
        {
            StartGame();
        }
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.R))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }

    void OnConnect(int device_id)
    {
        Debug.Log("Connected: " + device_id);
        AirConsole.instance.SetActivePlayers(AirConsole.instance.GetControllerDeviceIds().Count);

        //if (AirConsole.instance.GetActivePlayerDeviceIds.Count == 0)
        //{
        //    if (AirConsole.instance.GetControllerDeviceIds().Count >= PlayersRequired)
        //    {
        //        Debug.Log("Starting Game");
        //        StartGame();
        //    }
        //    else
        //    {
        //        //uiText.text = "NEED MORE PLAYERS";
        //    }
        //}
    }

    void OnMessage(int device_id, JToken data)
    {
        if (PlayerController == null)
            PlayerController = GameObject.Find("PlayerManager").GetComponent<PlayerController>();

        var active_player = AirConsole.instance.ConvertDeviceIdToPlayerNumber(device_id);
        if (active_player != -1)
        {
            if (data.ToString().Contains("joystick-left"))
            {
                // MOVE
                if ((bool) data["joystick-left"]["pressed"])
                {
                    var x = (float)data["joystick-left"]["message"]["x"];
                    var y = -(float)data["joystick-left"]["message"]["y"];
                    PlayerController.PlayerMoved(active_player, x, y);
                }
                else
                {
                    // Stop Moving
                    PlayerController.PlayerMoved(active_player,0f,0f);
                }
            }
            if (data.ToString().Contains("Jump") && (bool)data["Jump"]["pressed"])
            {
                // JUMP
                if (active_player == 0 && !GameManager.GameStarted)
                {
                    StartGame();
                }
                else
                {
                    PlayerController.JumpPressed(active_player);
                }
            }
            if (data.ToString().Contains("Place") && (bool)data["Place"]["pressed"])
            {
                // LOCK
                PlayerController.LockPressed(active_player);
            }   
        }
    }

    void StartGame()
    {
        if (PlayerController == null)
            PlayerController = GameObject.Find("PlayerManager").GetComponent<PlayerController>();
        for (int i = 0; i < AirConsole.instance.GetControllerDeviceIds().Count; i++)
        {
            PlayerController.SetPlayerBlock(i);
        }
        AirConsole.instance.SetActivePlayers(AirConsole.instance.GetControllerDeviceIds().Count);
        GameManager.StartGame(AirConsole.instance.GetControllerDeviceIds().Count);
    }

}
