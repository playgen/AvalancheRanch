﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public GameObject PlayerBlock;

    public GameObject SpawnLeft;
    public GameObject SpawnRight;

    private Dictionary<int, BlockControl> _playerBlocks = new Dictionary<int, BlockControl>();

    // Keep track of which player controls which object
    public void SetPlayerBlock(int playerNum)
    {
        var spawnPos = playerNum % 2 == 0 ? SpawnLeft : SpawnRight;
        var zOffset = Random.Range(-3f, 3f);
        var xOffset = Random.Range(-1f, 1f);
        Debug.Log(PlayerBlock);
        var block = Instantiate(PlayerBlock, spawnPos.transform.position + new Vector3(xOffset,1f,zOffset), Quaternion.identity);
        var control = block.GetComponent<BlockControl>();
        control.Player = playerNum;
        _playerBlocks.Add(playerNum, control);
    }

    public void NewBlock(int playerNum)
    {
        // Remove the current block from the dictionary
        _playerBlocks.Remove(playerNum);

        // Add a new Block
        SetPlayerBlock(playerNum);
    }

    // Handle Air Console Controls
    public void PlayerMoved(int playerNum, float x, float y)
    {
        var block = GetBlock(playerNum);

        if (block != null)
        {
            block.UpdateDirection(x, y);
        }
    }

    public void LockPressed(int playerNum)
    {
        var block = GetBlock(playerNum);

        var validPosition = block.transform.position.x > -5f && block.transform.position.x < 5f;

        if (block != null && validPosition)
        {
            block.LockPressed = true;
            NewBlock(playerNum);
        }
    }

    public void JumpPressed(int playerNum)
    {
        var block = GetBlock(playerNum);

        if (block != null)
        {
            block.JumpPressed = true;
        }
    }

    private BlockControl GetBlock(int player)
    {
        // Get the block the player is controlling
        BlockControl block = null;
        _playerBlocks.TryGetValue(player, out block);

        return block;
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.A))
        {
            PlayerMoved(0, -1f, 0f);
        }
        if (Input.GetKey(KeyCode.D))
        {
            PlayerMoved(0, 1f, 0f);
        }
        if (Input.GetKey(KeyCode.S))
        {
            PlayerMoved(0, 0f, -1f);
        }
        if (Input.GetKey(KeyCode.W))
        {
            PlayerMoved(0, 0f, 1f);
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            JumpPressed(0);
        }
        if (Input.GetKeyDown(KeyCode.KeypadEnter))
        {
            LockPressed(0);
        }
    }
      
}
