﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockControl : MonoBehaviour
{
    public float MovementSpeed = 1f;
    public float JumpHeight = 50f;

    [HideInInspector] public int Player;

    private float _xDirection;
    private float _yDirection;

    public bool JumpPressed { get; set; }
    public bool LockPressed { get; set; }

    public Material LockedMaterial;

    private Rigidbody _rigidbody;

    void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

	// Update is called once per frame
	void LateUpdate () {
		Move();
	    if (JumpPressed)
	    {
            JumpPressed = false;
	        Jump();
	    }
	    if (LockPressed)
	    {
	        LockPressed = false;
	        Lock();
	    }


	    
    }

    public void UpdateDirection(float x, float y)
    {
        _xDirection = x;
        _yDirection = y;
    }

    private void Move()
    {
        _rigidbody.AddForce(new Vector3(_xDirection, 0f, _yDirection) * MovementSpeed);
    }

    private void Jump()
    {
        //_rigidbody.AddForce(new Vector3(0f, 1f, 0f) * JumpHeight);
        _rigidbody.AddExplosionForce(JumpHeight, new Vector3(transform.position.x, transform.position.y - (transform.localScale.y/2), transform.position.z), 1f);
    }

    private void Lock()
    {
        _xDirection = 0f;
        _yDirection = 0f;
        _rigidbody.velocity = Vector3.zero;
        transform.GetChild(0).GetComponent<Renderer>().material = LockedMaterial;
        GetComponent<Rigidbody>().mass *= 5;
        Destroy(this);
    }
}
