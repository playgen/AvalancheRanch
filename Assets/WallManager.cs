﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallManager : MonoBehaviour {

    public int NumberOfBlocks
    {
        get { return _blockStartingPositions.Count; }
    }

    public int NumberInStartingPlace
    {
        get
        {
            var inPlace = 0;
            foreach (var blockStartingPosition in _blockStartingPositions)
            {
                if (Vector3.Distance(blockStartingPosition.Key.transform.position, blockStartingPosition.Value) <
                    _distanceOffsetAllowance)
                {
                    inPlace++;
                }
            }

            return inPlace;
        }
    }

    // The distance from the starting point a block must be to be considered out of position
    private float _distanceOffsetAllowance = 0.25f;

    private Dictionary<GameObject, Vector3> _blockStartingPositions = new Dictionary<GameObject, Vector3>();

    void Start()
    {
        var childBlocks = transform.GetComponentsInChildren<Transform>();

        foreach (var childBlock in childBlocks)
        {
            _blockStartingPositions.Add(childBlock.gameObject, childBlock.transform.position);
        }
    }

}
