﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RanchManager : MonoBehaviour {

    private List<WallManager> _walls = new List<WallManager>();

    void Start()
    {
        var children = GetComponentsInChildren<WallManager>();
        foreach (var child in children)
        {
            _walls.Add(child);
        }
    }

    public int GetStatus()
    {

        var totalPieces = 1;
        var totalInPlace = 1;

        foreach (var wallManager in _walls)
        {
            totalPieces += wallManager.NumberOfBlocks;
            totalInPlace += wallManager.NumberInStartingPlace;
        }

        var status = (float) totalInPlace / (float) totalPieces;
        status *= 100;

        return Mathf.RoundToInt(status);
    }
}
